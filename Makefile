CC = gcc
FLEX = flex
BISON = bison
BFLAGS = -d -v

run:
 
	${BISON} ${BFLAGS} yacc.y
	${FLEX} lex.l
	${CC} yacc.tab.c
 
clean:
	rm yacc.output yacc.tab.c yacc.tab.h lex.yy.c a.out *.java
