%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "lex.yy.c"

FILE *arq;
char Str[100];
char NameClass[100];
char visibility[100];
char type[100];
int result;
char * tab;
int qtdClasses = 0;


typedef struct _Param{

	char * name;
	char * type;

}Param;

typedef struct _Methods{

	char * name;
	char * visibility;
	char * returnType;
	Param * params;

}Methods;

typedef struct _Attributes{

	char * name;
	char * visibility;
	char * type;
	bool hasGetter;
	bool hasSetter;

}Attributes;

typedef struct _Class{

	char * name;
	char * id;
	int qtdAttributes;
	int qtdMethods;
	Attributes * attributes;
	Methods * methods;
	bool abstract;

}Class;

Class * classes;

%}

%union {
	char *strdata;
}
 
%token T_STRING
%token T_DIGIT
%token T_FROM
%token T_OPENCLASS 
%token T_CLOSECLASS 
%token T_OPENATRIBUTE 
%token T_CLOSEATRIBUTE
%token T_OPENMETHOD 
%token T_CLOSEMETHOD
%token T_CLOSEOPENTAG 
%token T_NAME
%token T_ATTRIBUITION
%token T_VISIBILITY
%token T_OPENTYPE
%token T_CLOSETYPE
%token T_OPENRETURNTYPE
%token T_CLOSERETURNTYPE
%token T_OPENPARAMETER
%token T_CLOSEPARAMETER
%token T_OPENDATATYPE
%token T_CLOSE
%token T_ISABSTRACT
%token T_HASGETTER
%token T_HASSETTER
%token T_ID
%token T_CODID

 
%%
 
stmt:
		openClass field_list_class T_CLOSEOPENTAG atribute_list method_list closeClass {}
		|stmt openClass field_list_class T_CLOSEOPENTAG atribute_list method_list closeClass


openClass:
		T_OPENCLASS {
		}

closeClass:
		T_CLOSECLASS{
			/*result = fputs("\n}", arq);
			if (result == EOF)
    			printf("Erro na Gravacao\n");
			fclose(arq);*/
			
		}
 
atribute_list:
		atribute_stmt 
	|	atribute_list atribute_stmt {}
		
method_list:
		method_stmt 
	|	method_list method_stmt {}
		


atribute_stmt:
		T_OPENATRIBUTE field_list_attribute T_CLOSEOPENTAG tag_type_attribute T_CLOSEATRIBUTE {}

method_stmt:
		T_OPENMETHOD field_list_method T_CLOSEOPENTAG tag_returnType_method name_type_parameter_method T_CLOSEMETHOD {}


field_list_class:
		field_class 
	|	field_list_class ' ' field_class
 

field_class:
		T_ISABSTRACT T_ATTRIBUITION T_STRING T_ID T_ATTRIBUITION T_CODID T_NAME T_ATTRIBUITION T_STRING {
				
			if (!classes){
			qtdClasses++;	
				classes = (Class *) malloc (qtdClasses* sizeof (Class));
				classes->name = $<strdata>9;
				classes->id = $<strdata>6;
				classes->qtdAttributes = 0;
				classes->qtdMethods = 0;
				if (strcmp("true", $<strdata>3)==0){
					classes->abstract = true;
				}else{
					classes->abstract = false;
				}

			}
			else{
			qtdClasses++;
				Class * temp = (Class *) malloc (qtdClasses* sizeof (Class));
				temp = classes;
				classes = (Class *) malloc (qtdClasses* sizeof (Class));
				classes = temp;
				classes[qtdClasses-1].name = $<strdata>9;
				classes[qtdClasses-1].id = $<strdata>6;
				classes[qtdClasses-1].qtdAttributes = 0;
				classes[qtdClasses-1].qtdMethods = 0;
				
				if (strcmp("true", $<strdata>3)==0){
					classes[qtdClasses-1].abstract = true;
				}else{
					classes[qtdClasses-1].abstract = false;
				}
				
			}
			/*strcpy(NameClass, yytext);
			arq = fopen(strcat(NameClass,".java"), "wt"); 
			if (arq == NULL){
    			printf("Problemas na CRIACAO do arquivo\n");
    			return;
			}
			strcpy(Str, "public class ");
			strcat(Str,yytext);
			strcat(Str,"{\n\n");
			result = fputs(Str, arq);
			if (result == EOF)
    			printf("Erro na Gravacao\n");*/
			}

field_list_attribute:
		field_attribute 
 

field_attribute:
		name_attribute visibility_attribute field_getset



field_list_method:
		field_method

field_method:
		name_method visibility_method


field_getset:
	T_HASGETTER T_ATTRIBUITION T_STRING T_HASSETTER T_ATTRIBUITION T_STRING{

				if (strcmp("true", $<strdata>3)==0){
					classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].hasGetter  = true;
				}else{
					classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].hasGetter  = false;

				}
				if (strcmp("true", $<strdata>6)==0){
					classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].hasSetter  = true;
				}else{
					classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].hasSetter  = false;

				}
			
	}


name_attribute:
		T_NAME T_ATTRIBUITION T_STRING {
			printf("passou 1\n");
			if (classes[qtdClasses-1].qtdAttributes == 0){
				classes[qtdClasses-1].qtdAttributes++;
				classes[qtdClasses-1].attributes = (Attributes *) malloc (classes[qtdClasses-1].qtdAttributes * sizeof (Attributes));
				classes[qtdClasses-1].attributes->name = $<strdata>3;
			}

			else{
				classes[qtdClasses-1].qtdAttributes++;
				Attributes * temp = (Attributes *) malloc (classes[qtdClasses-1].qtdAttributes * sizeof (Attributes));
				temp = classes[qtdClasses-1].attributes;
				classes[qtdClasses-1].attributes = (Attributes *) malloc (classes[qtdClasses-1].qtdAttributes * sizeof (Attributes));
				classes[qtdClasses-1].attributes = temp;
				classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].name = $<strdata>3;
			}

			//strcpy(Str, yytext);
		}

visibility_attribute:
		T_VISIBILITY T_ATTRIBUITION T_STRING {
			printf("passou 2\n");
			printf("VISIBILITYYYY    %s\n", $<strdata>3 );
			classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].visibility = $<strdata>3;
			printf("VISIBILITYYYY CLASSSS   %s\n", classes[0].attributes[0].visibility );

			/*strcpy(visibility, yytext);
			strcat(visibility," ");*/
			
			
		}
tag_type_attribute:
	T_OPENTYPE T_CLOSEOPENTAG T_OPENDATATYPE type_attribute T_CLOSEOPENTAG T_CLOSETYPE

type_attribute:
	T_NAME T_ATTRIBUITION T_STRING{
		
		if (!strcmp("string", $<strdata>3)){
			strcpy($<strdata>3, "String");
		}

		classes[qtdClasses-1].attributes[classes[qtdClasses-1].qtdAttributes-1].type = $<strdata>3;
		/*
		strcat(type, " ");
		strcpy (attribute_declaration, "\t");
		strcat(attribute_declaration,visibility);
		strcat(attribute_declaration,type);
		strcat(attribute_declaration,Str);
		result = fputs(strcat(attribute_declaration, ";\n"), arq);
			if (result == EOF)
    			printf("Erro na Gravacao\n");*/
	}

name_method:
		T_NAME T_ATTRIBUITION T_STRING {

			if (classes[qtdClasses-1].qtdMethods == 0){
				classes[qtdClasses-1].qtdMethods++;
				classes[qtdClasses-1].methods = (Methods *) malloc (classes[qtdClasses-1].qtdMethods * sizeof (Methods));
				classes[qtdClasses-1].methods->name = $<strdata>3;
			}

			else{
				classes[qtdClasses-1].qtdMethods++;
				Methods * temp = (Methods *) malloc (classes[qtdClasses-1].qtdMethods * sizeof (Methods));
				temp = classes[qtdClasses-1].methods;
				classes[qtdClasses-1].methods = (Methods *) malloc (classes[qtdClasses-1].qtdMethods * sizeof (Methods));
				classes[qtdClasses-1].methods = temp;
				classes[qtdClasses-1].methods[classes[qtdClasses-1].qtdMethods-1].name = $<strdata>3;
			}

			//strcpy(Str, yytext);
		}

visibility_method:
		T_VISIBILITY T_ATTRIBUITION T_STRING {

			classes[qtdClasses-1].methods[classes[qtdClasses-1].qtdMethods-1].visibility = $<strdata>3;			
			
		}
tag_returnType_method:
	T_OPENRETURNTYPE T_CLOSEOPENTAG T_OPENDATATYPE return_type_method T_CLOSEOPENTAG T_CLOSERETURNTYPE


return_type_method: 
	T_NAME T_ATTRIBUITION T_STRING{
		
		if (!strcmp("string", $<strdata>3)){
			strcpy($<strdata>3, "String");
		}

		classes[qtdClasses-1].methods[classes[qtdClasses-1].qtdMethods-1].returnType = $<strdata>3;
		
	}


name_type_parameter_method:
	tag_name_parameter_method tag_type_parameter_method

tag_name_parameter_method:
	T_OPENPARAMETER name_parameter_method T_CLOSEOPENTAG T_CLOSEPARAMETER

name_parameter_method:
	T_NAME T_ATTRIBUITION T_STRING{

		printf("NOME DO PARAMETRO DO METODO: %s\n", $<strdata>3);	

		classes[qtdClasses-1].methods[classes[qtdClasses-1].qtdMethods-1].params->name = $<strdata>3;
			
	}

tag_type_parameter_method:
	T_OPENTYPE T_CLOSEOPENTAG T_OPENDATATYPE type_parameter_method T_CLOSEOPENTAG T_CLOSETYPE

type_parameter_method:
	T_NAME T_ATTRIBUITION T_STRING{

		printf("TIPO DO PARAMETRO DO METODO: %s\n", $<strdata>3);
			
		if (!strcmp("string", $<strdata>3)){
			strcpy($<strdata>3, "String");
		}	

		classes[qtdClasses-1].methods[classes[qtdClasses-1].qtdMethods-1].params->type = $<strdata>3;
			
	}


%%
 
int yywrap(void) { 
	return 1; 
}

int yyerror(char *s) {
	printf("%s\n",s);
}

void writeFile (char * text){

	result = fputs(text, arq);
		if (result == EOF)
			printf("Erro na Gravacao\n");

}

void printSyntaxTree(){

	int i;
	for (i = 0; i <qtdClasses; i++){
		printf("Name of Class: %s\n", classes[i].name);
		printf("Is Abstract?: %d\n", classes[i].abstract);
		printf("ID: %s\n", classes[i].id);
		printf("qtdAttributes: %d\n", classes[i].qtdAttributes);
		printf("Atrributes:\n");
		int j;
		for (j=0; j<classes[i].qtdAttributes; j++){
			printf ("\t Name = %s\n", classes[i].attributes[j].name);
			printf ("\t Visibility = %s\n", classes[i].attributes[j].visibility);
			printf ("\t Type =  %s\n", classes[i].attributes[j].type);

			printf ("\t getter = %d\n", classes[i].attributes[j].hasGetter);
			printf ("\t setter = %d\n", classes[i].attributes[j].hasSetter);
			printf ("\n");
		}
		printf("Methods:\n");
		printf("qtdMethods: %d\n", classes[i].qtdMethods);
		for (j=0; j<classes[i].qtdMethods; j++){
			printf ("\t Name = %s\n", classes[i].methods[j].name);
			printf ("\t Visibility = %s\n", classes[i].methods[j].visibility);
			printf ("\t returnType =  %s\n", classes[i].methods[j].returnType);

			printf ("\n");
		}
	}
}

int main(void)
{

	char * nameArq;

     yyparse();
     printSyntaxTree();
	int i, j;
	for (i = 0; i<qtdClasses;i++){	
		nameArq = strdup(classes[i].name);
		strcat(nameArq, ".java");
		arq = fopen(nameArq, "wt"); 
		if (arq == NULL){
			printf("Problemas na CRIACAO do arquivo\n");
			return;
		}

		writeFile ("public ");
		if (classes[i].abstract){
			writeFile("abstract ");
		}
		writeFile ("class ");
		writeFile (classes[i].name);
		writeFile (" {\n");


		printf ("%s\n", classes[i].name);
		printf ("%d\n", classes[i].abstract);

		for (j=0; j<classes[i].qtdAttributes ; j++){
			writeFile ("\n");
			writeFile ("\t");
			writeFile (classes[i].attributes[j].visibility);
			writeFile (" ");
			writeFile (classes[i].attributes[j].type);
			writeFile (" ");
			writeFile (classes[i].attributes[j].name);
			writeFile (";");


			printf ("nome do atributo = %s\n", classes[i].attributes[j].name);
			printf ("visibility do atributo = %s\n", classes[i].attributes[j].visibility);
			printf ("type do atributo = %s\n", classes[i].attributes[j].type);

			printf ("getter = %d\n", classes[i].attributes[j].hasGetter);
			printf ("setter = %d\n", classes[i].attributes[j].hasSetter);


		}

		writeFile ("\n");
		for (j=0; j<classes[i].qtdAttributes ; j++){
			
			if (classes[i].attributes[j].hasGetter){
				writeFile ("\n");
				writeFile ("\t");
				writeFile ("public ");
				writeFile (classes[i].attributes[j].type);
				writeFile (" get");
				writeFile (classes[i].attributes[j].name);
				writeFile ("(){\n\t\treturn this.");
				writeFile (classes[i].attributes[j].name);
				writeFile (";");
				writeFile ("\n\t}");

			}

			if (classes[i].attributes[j].hasSetter){
				writeFile ("\n");
				writeFile ("\t");
				writeFile ("public void ");
				writeFile ("set");
				writeFile (classes[i].attributes[j].name);
				writeFile ("(");
				writeFile (classes[i].attributes[j].type);
				writeFile (" ");
				writeFile (classes[i].attributes[j].name);
				writeFile (")");
				writeFile ("{\n\t\tthis.");
				writeFile (classes[i].attributes[j].name);
				writeFile (" = ");
				writeFile (classes[i].attributes[j].name);
				writeFile (";");
				writeFile ("\n\t}");

			}

		}

		printf("QUANTIDADE DE METODOS: %d\n", classes[i].qtdMethods);
		for (j=0; j<classes[i].qtdMethods ; j++){
			
			if (classes[i].qtdMethods){
			writeFile ("\n");
			writeFile ("\t");
			writeFile (classes[i].methods[j].visibility);
			writeFile (" ");
			writeFile (classes[i].methods[j].returnType);
			writeFile (" ");
			writeFile (classes[i].methods[j].name);
			writeFile (" ");
			writeFile ("(){\n\t\t");
			writeFile ("\n\t}");


			printf ("nome do Metodo = %s\n", classes[i].methods[j].name);
			printf ("visibility do metodo = %s\n", classes[i].methods[j].visibility);
			printf ("type do metodo = %s\n", classes[i].methods[j].returnType);
			}

		}
	printf ("\n");
	writeFile("\n");
	writeFile ("}");

	}
	/*
	*/

     return 0;
}